import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import { routes } from './routes'
import store from './store'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { VueHammer } from 'vue2-hammer'

Vue.config.productionTip = false

Vue.use(VueRouter)

const router = new VueRouter({ mode: 'history', routes })

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueHammer)

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
