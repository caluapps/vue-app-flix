import HomeComponent from './components/HomeComponent'
import CardComponent from './components/CardComponent'

export const routes = [
	{ path: '', component: HomeComponent },
	{ path: '/cards/:id', component: CardComponent }
]