const state = {
	flixcards: [
		{
			id: 0,
			title: 'start a vue project',
			description: 'learn and memorize how to start a vue project',
			createdBy: 'user',
			flashcardset: [
				{
					cardQuestion: 'what are the software requirements for vue?',
					cardAnswer: 'sometimes node.js and npm'
				}, {
					cardQuestion: 'why is vue so popular',
					cardAnswer: 'node.js and npm'
				}
			],
			tags: ['JavaScript', 'vue.js', 'install']
		},	{
			id: 1,
			title: 'create a vue component',
			description: 'how to create a vue compoent',
			createdBy: 'user',
			flashcardset: [
				{
					cardQuestion: 'Which tags do you need in your component',
					cardAnswer: 'template, script and style'
				}, {
					cardQuestion: 'Is it neccessary to have script and styles in your vue component?',
					cardAnswer: 'It is possible to export the script and style into sepparate files but it is recommended by vue to stick them in one file.'
				}
			],
			tags: ['JavaScript', 'vue.js', 'create comopnent']
		}, {
			id: 2,
			title: 'vue and routes',
			description: 'all about router and routes in vue',
			createdBy: 'user',
			flashcardset: [
				{
					cardQuestion: 'where to put the path and the linking to the specific component',
					cardAnswer: 'router has to be declared in main.js and the routes can be declared in main.js aswell but it is also possible to store it in a separate file named routes.js'
				}, {
					cardQuestion: 'how to does a router link look like?',
					cardAnswer: 'Todo: answer this'
				}
			],
			tags: ['JavaScript', 'vue.js', 'create comopnent']
		}
	]
}

const getters = {
	getFlixcards: () => state.flixcards
}

const actions = { }

const mutations = { }

export default {
	state,
	getters,
	actions,
	mutations
}